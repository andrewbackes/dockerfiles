# consul-windows

Runs a Consul Agent with the Web UI in a Windows Docker Container

## Build Container

```
docker build -t consul .
```

## Run Container

```
docker run -d -p 8500:8500 consul
```

Once running, you can browse to http://localhost:8500 to view the Consul Web UI.