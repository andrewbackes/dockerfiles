# dotnet-nodejs

Container with the latest dotnet sdk and node v10.x

## Build Container

```
docker build -t dotnet-nodejs .
```

## Run Container

```
docker run -d dotnet-nodejs
```